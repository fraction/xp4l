Xp-For-Live, aka xp4l®

xp4l is a fully integrated solution designed to expand with simplicity Ableton Live potential toward the field of spatial sound design and live performance. 

The goal of xp4l is to provide Ableton users with a flexible and simplified environment to quickly create 3d audio projects.

It is made of a free m4l (max-for-live) suit consisting of 5 devices and a standalone application that users have to purchase. We use to call familiarly this package xP.

xP has been created by Eric Raynaud aka Fraction (https://www.fractionisnoise.art)


Information on compatibility and requirements are accessible at https://www.xp4l.com

Have Fun!